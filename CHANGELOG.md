## 1.0.2

#### Fixed
#### Changes
#### Additions

## 1.0.1

#### Fixed
#### Changes
#### Additions

- Added redirect_uri configuration logic


## 1.0.0

#### Fixed
#### Changes
#### Additions


[app-demo]: app-demo
[tinkoff-id]: tinkoff-id
